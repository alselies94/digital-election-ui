import { Presence } from './../presence';
import { StudentService } from './../student.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Student } from '../student';

@Component({
  selector: 'app-attend',
  templateUrl: './attend.component.html'
})
export class AttendComponent implements OnInit {

  message: string;
  color: string;

  constructor(private studentService: StudentService, private router: Router) { }

  ngOnInit() {
    this.color = 'green';
  }

  onSubmit(data) {

    if ( data.value.nim.length === 10 ) {

      this.studentService.absenceVote(data.value.nim, data.value.votecode)
      .subscribe( (res: Presence) => {

        if ( res.canVote ) {
          this.color = 'green';

          this.studentService.getStudentByNIM(data.value.nim)
          .subscribe((st : Student) => {
            this.message = 'Hay ' + st.name + ', you can vote now';
            setTimeout(() => this.router.navigate(['/CrEtTaUUvGlWCIiqoITpKY2cSSG33FTWwesSoG1VNNaMtRr5rRH8K4ehKc4arM177Faezm3XCdifgLJqFEuLEQ4gIapGqUhp8iqryf9A1xikt4kHUXtBjNI30nvpYQDOUC9xfiwkDzXqKhiZ9uHksRasoS7CrarsjnZVRNNlz2V2XjFZb19bXGLozDgq7nmBngMLkzoZ'], {queryParams: {nim: data.value.nim}}), 2000);
          });

        }

      }, (res) => {
        this.color = 'red';
        this.message = res.error;
      });
    } else {
      this.message = 'NIM not valid';
      this.color = 'red';
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
