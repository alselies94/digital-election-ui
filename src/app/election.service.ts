import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Candidate } from './candidate';
import { ChartResult } from './chartresult';

@Injectable({
  providedIn: 'root'
})


export class ElectionService {
  private url: string;
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {

    const config = window['config']();
    this.url = config.SERVICE;
    this.headers = new HttpHeaders({
        'Authorization': config.TOKEN,
        'Content-Type': 'application/json'
    });
   }

  generatePasscode(mastercode): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

    return this.http.post<any>(`${this.url}/election/generate?mastercode=${mastercode}`, null, httpOptions);
  }

  insertPasscode(passcode): Observable<ChartResult> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<ChartResult>(`${this.url}/election/result?passcode=${passcode}`, null, httpOptions);
  } 

  makeElection(nim, candidate): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<any>(`${this.url}/election/${nim}/${candidate}`, null, httpOptions);
  }

  getThisYearCandidate(): Observable<Candidate[]>{
    return this.http.get<Candidate[]>(`${this.url}/election/candidate`);
  }
}
