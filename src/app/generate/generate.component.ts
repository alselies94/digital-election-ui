import { ElectionService } from './../election.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-generate',
  templateUrl: './generate.component.html'
})

export class GenerateComponent implements OnInit {

  message: string;
  color: string;

  constructor(private electionService: ElectionService, private router: Router) { }

  ngOnInit() {
    this.color = 'green';
  }

  onSubmit(data) {

    this.electionService.generatePasscode(data.value.mastercode)
    .subscribe( (res: any) => {
      this.message = res.message;
      this.color = 'green';
    }, (res) => {
      this.message = res.error;
      this.color = 'red';
    });
  }

  generate() {
    this.router.navigate(['/dWEKdPuG5sWDxDXhPXUcoHkMPyZeD0fawY1xXl1SkQEPK6wc8cWkF9nIMliW7mJgyzMckZGYqXYbbzcwHLqd9L4Z2dEbtEDxVsfWvmqEPiyNaWeYtmkjNCQ1YsWJb1dqE4aJ5UD10UvGTBoyD4DIX9eODW8yMapWvNlpiTYayNeKqD9R5gSj9mlwuO8Qi5WS2AeQrYlg']);
  }

}
