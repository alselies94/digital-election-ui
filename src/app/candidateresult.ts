import { Candidate } from './candidate';

export interface CandidateResult {
  candidate: Candidate;
  voting: number;
}
