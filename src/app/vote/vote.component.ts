import { Component, OnInit } from '@angular/core';
import { ElectionService } from './../election.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Candidate } from './../candidate';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html'
})

export class VoteComponent implements OnInit {

  candidateList: Candidate[];
  imgPath: string;
  nim: string;

  constructor(private electionService: ElectionService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.electionService.getThisYearCandidate()
    .subscribe( (res: Candidate[]) => {
      this.candidateList = res;
    }, (res) => {
      alert(res.error);
      setTimeout(() => this.router.navigate(['/']), 1000);
    });

    this.route.queryParamMap.subscribe(params => {
      this.nim = params.get('nim');
      if ( this.nim == null ) {
        this.router.navigate(['/']);
      }
    });
    this.imgPath = 'assets/img/gallery/';
  }

  choose(candidateId) {
    this.electionService.makeElection(this.nim, candidateId)
    .subscribe((res: any) => {
      this.router.navigate(['/xlPAn9fkFYPA9rFp1Xl0f5rX8hK9N0Rm918yxc66DHdathC0jK0n1IOGn0nAwuWaAb892XAwRjbLMTbHcgx65Yje4CWqbF518NmCDx6SQI4kDcMVwB4Kgjwq77rPjHbVoz006mh8bvvxse2lcLBPG2MWI3uTzvsL6wLhgbrV1Hj2OqB0oxcUPQ9CH0dGuqe6eG3EBVsW']);
    }, (res) => {
      alert(res.error);
      this.router.navigate(['/frzgcSUct1XyqBkYKO9KNgK4XnGcCLYHafE3bOwQUlK2pXmFGM3U1UlilMysepEVoiNkpU94JaotU3RGrOW193UlqpPhvJdLvDFXopob3kJcO0SFQZTjb3v3cUxVDSfcQUrcrdmobNTKf1yvTR6bXH27nQ5vcWKkLzDKk2HI93g3bBReVwk0m0XdUEBNDf3uW3jYhdec']);
    });
  }

}
