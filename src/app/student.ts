export interface Student {
  nim: string;
  name: string;
  studentStatus: number;
  bapkmoStatus: number;
  bapkmoDate: Date;
  
}
