export interface Presence {
  canVote: boolean;
  response: string;
}
