import { SuccessComponent } from './success/success.component';
import { ResultComponent } from './result/result.component';
import { GenerateComponent } from './generate/generate.component';
import { ErrorComponent } from './error/error.component';
import { VoteComponent } from './vote/vote.component';
import { AttendComponent } from './attend/attend.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewStudentComponent } from './new-student/new-student.component';
import { ListUriComponent } from './list-uri/list-uri.component';


const routes: Routes = [
  { path : 'attend', component : AttendComponent },
  { path : 'home', component : HomeComponent },
  { path : 'vote', component : VoteComponent },
  { path : '', component : ErrorComponent },
  { path : 'list', component : ListUriComponent},
  { path : 'success', component : SuccessComponent },
  { path : 'generate', component : GenerateComponent },
  { path : 'result', component : ResultComponent },
  { path : 'student', component : NewStudentComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  HomeComponent,
  AttendComponent,
  VoteComponent,
  ErrorComponent,
  SuccessComponent,
  GenerateComponent,
  ResultComponent,
  NewStudentComponent,
  ListUriComponent
];
