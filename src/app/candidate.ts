export interface Candidate {
  id: number;
  leaderNIM: string;
  leaderName: string;
  photo: string;
  viceNIM: string;
  viceName: string;
  electionNumber: number;
  vision: string;
}
