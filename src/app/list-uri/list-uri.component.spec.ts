import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUriComponent } from './list-uri.component';

describe('ListUriComponent', () => {
  let component: ListUriComponent;
  let fixture: ComponentFixture<ListUriComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListUriComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
