import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-uri',
  templateUrl: './list-uri.component.html',
  styleUrls: ['./list-uri.component.css']
})
export class ListUriComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  home(){
    this.router.navigate(['/7aHtwnIy24aCGVgB1SV5znUDOKMNerIW07JY4FOXMrmlcxeulihNHilcv8ztFBVckCRZPyLTuGGLBqkciYpBIPUYQyKY5JFidwxLsZgUNGWyZqTXsqsi44PMqzFQjWk5o2VNhXg2qCuoQwPDShqHTW6p1XHxsGtUOsFPzvm5sNjr05s5Z3PMA2BPEqifOvbt2bFRxO8w']);
  }

  attend(){
    this.router.navigate(['/frzgcSUct1XyqBkYKO9KNgK4XnGcCLYHafE3bOwQUlK2pXmFGM3U1UlilMysepEVoiNkpU94JaotU3RGrOW193UlqpPhvJdLvDFXopob3kJcO0SFQZTjb3v3cUxVDSfcQUrcrdmobNTKf1yvTR6bXH27nQ5vcWKkLzDKk2HI93g3bBReVwk0m0XdUEBNDf3uW3jYhdec']);
  }

}
