import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Presence } from './../presence';
import { StudentService } from '../student.service';
import { Router } from '@angular/router';
import { Student } from '../student';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit, AfterViewInit {

  message: string;
  color: string;
  voteCode: string;
  @ViewChild('nimInput', {static: true}) nimView: ElementRef;

  constructor(private studentService: StudentService, private router: Router) { }

  ngAfterViewInit() {
    this.nimView.nativeElement.value = '';
  }

  ngOnInit() {
    this.color = 'green';
    console.log("test",environment.testASDASDASDASD);
  }

  onSubmit(data) {

    if ( data.value.nim.length === 10 ) {

      this.studentService.redeemVoteCode(data.value.nim)
      .subscribe( (res: Presence) => {

        if ( res.canVote ) {
          this.color = 'green';

          this.studentService.getStudentByNIM(data.value.nim)
          .subscribe((st : Student) => {
            this.message = 'Hay ' + st.name + ', please use your election right kindly';
            this.nimView.nativeElement.value = '';
            this.voteCode = res.response;
          });
          
        } else {
          this.color = 'red';
          this.message = res.response;
          this.voteCode = '';
        }

      },
      (res) => {
        this.color = 'red';
        this.message = res.error;
        this.voteCode = '';
      });
    } else {
      this.color = 'red';
      this.message = 'NIM not valid';
      this.voteCode = '';
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  generate() {
    this.router.navigate(['/ONtvb1g2D5L4vDEdtf6u834VPaINOYNcVaFIYlLp2yrp2aj56bSpW4s6jdUnVShOPvNKFmTXjF9hU6pwvzFxfsSuabjj2lMiYsJeC3koC4Z2PYkDnKVb2N7Kaib0Rv2zEbeECSq29h7PGwmAsLNHCdH940Z7Fxe493PT54BDHz3VrKM4wjqwtRp3x3hn4twkggEY9CDG']);
  }

}
