import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Presence } from './presence';
import { Observable } from 'rxjs';
import { Student } from './student';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class StudentService {
  
  private url: string;
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {
    const config = window['config']();
    this.url = config.SERVICE;
    this.headers = new HttpHeaders({
        'Authorization': config.TOKEN,
        'Content-Type': 'application/json'
    });
  }

  redeemVoteCode(nim): Observable<Presence>{
    return this.http.get<Presence>(`${this.url}/student/redeem/${nim}`);
  }

  absenceVote(nim, voteCode): Observable<Presence>{
    return this.http.get<Presence>(`${this.url}/student/${nim}?voteCode=${voteCode}`);
  }

  getStudentByNIM(nim): Observable<Student>{
    return this.http.get<Student>(`${this.url}/student/nim/${nim}`);
  }

  addNewStudent(student): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    
    return this.http.put<any>(`${this.url}/student`, student, httpOptions);
  }
}
