import { Component, OnInit } from '@angular/core';
import { Select2OptionData } from 'ng-select2';
import { StudentService } from '../student.service';
import { Student } from '../student';
import { Presence } from '../presence';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-student',
  templateUrl: './new-student.component.html',
  styleUrls: ['./new-student.component.css']
})
export class NewStudentComponent implements OnInit {

  studentStatus: Array<Select2OptionData>;
  bapkmoStatus: Array<Select2OptionData>;
  studentValue: string;
  bapkmoValue: string;
  myDate: Date;
  studentSubmit: Student;
  message: string;
  color: string;

  constructor(private studentService: StudentService, private router: Router) { }

  ngOnInit() {
    this.message = '';
    this.color = 'green';
    this.studentSubmit = {nim:'',name:'',bapkmoStatus:0, bapkmoDate:null, studentStatus:0};
    this.studentStatus = [
      {
        id: '0',
        text: 'Active'
      },
      {
        id: '1',
        text: 'Leave'
      },
      {
        id: '2',
        text: 'Out'
      },
      {
        id: '3',
        text: 'Graduate'
      }
    ];

    this.bapkmoStatus = [
      {
        id: '0',
        text: 'Not Yet'
      },
      {
        id: '1',
        text: 'Achieved'
      }
    ];
    
    this.bapkmoValue='0';
    this.studentValue='0';
    this.myDate = new Date();
  }

  onSubmit( data ) {

    this.studentSubmit = { nim:data.value.nim, name:data.value.name, bapkmoStatus: data.value.bapkmoStatus, 
      bapkmoDate:data.value.bapkmoDate, studentStatus:data.value.studentStatus };
    
    if(this.checkStudent()) {
      this.studentService.addNewStudent(this.studentSubmit)
      .subscribe((res: any) => {
        this.color = 'green';
        this.message = res.message;
      },(res) => {
        this.color = 'red';
        this.message = res.error;
      });
    }else{
      this.color = 'red';
      this.message = 'Please fill form completely';
    }
  }

  numberOnly( event ): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  checkStudent(): boolean {
    if(this.studentSubmit.nim.length == 0 || this.studentSubmit.name.length == 0){
      return false;
    }
    return true;
  }

  home() {
    this.router.navigate(['/7aHtwnIy24aCGVgB1SV5znUDOKMNerIW07JY4FOXMrmlcxeulihNHilcv8ztFBVckCRZPyLTuGGLBqkciYpBIPUYQyKY5JFidwxLsZgUNGWyZqTXsqsi44PMqzFQjWk5o2VNhXg2qCuoQwPDShqHTW6p1XHxsGtUOsFPzvm5sNjr05s5Z3PMA2BPEqifOvbt2bFRxO8w']);
  }
}
