import { ElectionService } from './election.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StudentService } from './student.service';
import { AttendComponent } from './attend/attend.component';
import { VoteComponent } from './vote/vote.component';
import { ErrorComponent } from './error/error.component';
import { GenerateComponent } from './generate/generate.component';
import { ResultComponent } from './result/result.component';
import { SuccessComponent } from './success/success.component';
import { ChartsModule } from 'ng2-charts';
import { NewStudentComponent } from './new-student/new-student.component';
import { NgSelect2Module } from 'ng-select2';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListUriComponent } from './list-uri/list-uri.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AttendComponent,
    VoteComponent,
    ErrorComponent,
    GenerateComponent,
    ResultComponent,
    SuccessComponent,
    NewStudentComponent,
    ListUriComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgSelect2Module,
    ChartsModule,
    BrowserAnimationsModule
  ],
  providers: [StudentService, ElectionService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
