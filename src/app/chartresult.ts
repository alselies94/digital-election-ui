import { CandidateResult } from './candidateresult';

export interface ChartResult {
  candidateResult: CandidateResult[];
  voteStatisticResult: any;
  totalActiveStudent: number;
}
