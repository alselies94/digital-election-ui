import { ElectionService } from './../election.service';
import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { SingleDataSet, Color, Label } from 'ng2-charts';
import { CandidateResult } from '../candidateresult';
import { ChartResult } from '../chartresult';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html'
})

export class ResultComponent implements OnInit {

  message: string;
  color: string;
  dataform: ChartResult;
  currentYear: Date = new Date();
  abstentions: number = 0;
  totalActiveStudent: number = 0;
  candidate: CandidateResult[];
  winner: number = 0;

  constructor(private electionService: ElectionService, private router: Router) { }

  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = ['Paslon 1', 'Paslon 2'];
  public pieChartData: SingleDataSet = [500, 500];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  //line
  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Voter' },
  ];
  public lineChartLabels: Label[] = ['09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00'];
  public lineChartOptions: ChartOptions = {
    responsive: true, maintainAspectRatio: false,
  };

  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,151,0,0.6)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  ngOnInit() {
    this.color = 'green';
  }

  onSubmit(data) {

    this.electionService.insertPasscode(data.value.passcode)
    .subscribe( (res: ChartResult) => {

      this.dataform = res;
      this.message = 'Thankyou #Respect';
      this.color = 'green';
      
      //line statistic
      let lineST = res.voteStatisticResult;
      let lineSTX = [];
      let lineSTY = [];

      for(let x in lineST){
        lineSTY.push(lineST[x]);
        lineSTX.push(x+':00');
      }

      this.lineChartLabels = lineSTX;
      this.lineChartData = [
        { data: lineSTY, label: 'Voter' },
      ];

      //pie statistic
      let pieST = res.candidateResult;
      this.candidate = res.candidateResult;
      let pieSTLabel = [];
      let pieSTValue = [];
      let total = 0;

      for(let c in pieST){
        pieSTLabel.push('Paslon ' + pieST[c].candidate.electionNumber);
        pieSTValue.push(pieST[c].voting);
        if(pieST[c].voting > this.winner) this.winner = pieST[c].voting
        total += pieST[c].voting;
      }

      this.totalActiveStudent = res.totalActiveStudent;
      this.abstentions = this.totalActiveStudent - total;

      this.pieChartLabels = pieSTLabel;
      this.pieChartData = pieSTValue;

    }, (res: any) => {
      this.message = res.error;
      this.color = 'red';
    });

  }

  passcode() {
    this.router.navigate(['/ONtvb1g2D5L4vDEdtf6u834VPaINOYNcVaFIYlLp2yrp2aj56bSpW4s6jdUnVShOPvNKFmTXjF9hU6pwvzFxfsSuabjj2lMiYsJeC3koC4Z2PYkDnKVb2N7Kaib0Rv2zEbeECSq29h7PGwmAsLNHCdH940Z7Fxe493PT54BDHz3VrKM4wjqwtRp3x3hn4twkggEY9CDG']);
  }
}
