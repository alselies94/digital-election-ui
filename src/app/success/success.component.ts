import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {

    setTimeout(() => this.router.navigate(['/frzgcSUct1XyqBkYKO9KNgK4XnGcCLYHafE3bOwQUlK2pXmFGM3U1UlilMysepEVoiNkpU94JaotU3RGrOW193UlqpPhvJdLvDFXopob3kJcO0SFQZTjb3v3cUxVDSfcQUrcrdmobNTKf1yvTR6bXH27nQ5vcWKkLzDKk2HI93g3bBReVwk0m0XdUEBNDf3uW3jYhdec']), 2000);
  }

}
